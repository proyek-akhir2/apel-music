import React, { useEffect, useState } from "react";
import Navbar from "../assets/components/Navbar";
import { Box, Typography } from "@mui/material";
import userImg from "../assets/img/gray_user.png";

const ProfileField = ({ label, value }) => {
  return (
    <>
      <Box
        sx={{
          textAlign: "center",
        }}
      >
        <Typography fontSize={"1.5rem"} fontWeight={400}>
          {label}
        </Typography>
        <Typography fontSize={"1.25rem"} fontWeight={600}>
          {value}
        </Typography>
      </Box>
    </>
  );
};

const UserProfile = () => {
  const [userProfile, setUserProfile] = useState({
    id: "",
    name: "",
    email: "",
    role: "",
  });

  useEffect(() => {
    let id = localStorage.getItem("user_id");
    let name = localStorage.getItem("user_name");
    let email = localStorage.getItem("user_email");
    let role = localStorage.getItem("user_role");

    setUserProfile({ id, name, email, role });
  }, []);
  // TODO: Lanjut bikin add courses
  return (
    <>
      <Navbar />
      <Box
        sx={{
          maxWidth: "1980px",
          width: "100%",
          padding: "1.5rem",
          margin: "auto",
        }}
      >
        <Box
          sx={{
            backgroundColor: "#F6F7FF",
            width: "100%",
            borderRadius: "0.5rem",
            boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
            minHeight: "200px",
            paddingTop: "2rem",
            paddingBottom: "5rem",
          }}
        >
          {/* TODO: Lanjut bikin user profile */}
          <Box
            component={"img"}
            src={userImg}
            sx={{
              width: "100px",
              height: "100px",
              borderRadius: "100%",
              objectFit: "cover",
              objectPosition: "center top",
              margin: "auto",
              backgroundColor: "#E2E2E2",
            }}
          />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginTop: "2rem",
            }}
          >
            <Typography
              marginBottom={"2rem"}
              color={"#969696"}
              fontSize={"1rem"}
              fontWeight={400}
              textAlign={"center"}
            >
              Id: {userProfile.id}
            </Typography>
            <Typography fontSize={"2rem"} fontWeight={600}>
              {userProfile.name}
            </Typography>
            <Typography
              marginBottom={"2rem"}
              color={"#969696"}
              fontSize={"1rem"}
              fontWeight={400}
            >
              {userProfile.email}
            </Typography>
            <Typography color={"#969696"} fontSize={"1rem"} fontWeight={400}>
              Hak akses
            </Typography>
            <Typography fontSize={"1.5rem"} fontWeight={600}>
              {userProfile.role}
            </Typography>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default UserProfile;
