import { Button, Switch, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { useEffect, useState } from "react";
import AppTable from "../../assets/components/AppTable";
import AdminUtility from "../../assets/components/admin/AdminUtility";
import Copyright from "../../assets/components/admin/Copyright";
import CommonDialog from "../../assets/components/dialogs/CommonDialog";
import { useApiContext } from "../../context/ApiProvider";

// TODO remove, this demo shouldn't need to reset the theme.

export default function AdminPayment() {
  const [open, setOpen] = React.useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const [add, setAdd] = React.useState(false);

  const handleCloseAddPayment = () => {
    setAdd(false);
  };

  const columnTable = ["Image", "Name", "Active", "Action"];

  const [paymentRows, setPaymentRows] = useState([]);

  const [pageSize, setPageSize] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const { AdminServices, URLs } = useApiContext();

  const [image, setImage] = useState(null);
  const [editImage, setEditImage] = useState(null);
  const [editedId, setEditedId] = useState("");
  const [paymentName, setPaymentName] = useState("");
  const [openAddPayment, setOpenAddPayment] = useState(false);
  const [openEditPayment, setOpenEditPayment] = useState(false);
  const [paymentActive, setPaymentActive] = useState(false);

  const [fieldErrors, setFieldErrors] = useState({
    Name: [],
    Image: [],
  });

  const [nonFieldErrors, setNonFieldErrors] = useState(false);

  const convertPaymentToRow = (payment) => {
    return {
      Image: (
        <Box
          component={"img"}
          src={URLs.IMG_URL + payment?.image}
          sx={{
            height: { xs: "4rem", sm: "4rem", md: "3rem" },
            width: { xs: "4rem", sm: "4rem", md: "3rem" },
            borderRadius: "100%",
            margin: "auto",
          }}
        />
      ),
      Name: payment?.name,
      Status: (
        <Button
          variant="contained"
          color={payment?.inactive ? "error" : "success"}
          sx={{ width: "5rem" }}
        >
          {payment?.inactive ? "Inactive" : "Active"}
        </Button>
      ),
      Action: (
        <>
          <Button
            variant="contained"
            sx={{
              minWidth: "7rem",
            }}
            onClick={() => handleEdit(payment?.id)}
          >
            Edit
          </Button>
        </>
      ),
    };
  };

  // TODO: Rapihin lagi untuk semua validasi, termasuk yang dari user, sama tambahin add new course
  const prepareEditField = (paymentId) => {
    AdminServices.getPaymentDetail(paymentId)
      .then((res) => {
        let result = res?.data;
        setPaymentName(result?.name);
        setEditImage(URLs.IMG_URL + result?.image);
        setPaymentActive(!result?.inactive);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (paymentId) => {
    clearFields();
    clearErrorInfo();
    setEditedId(paymentId);
    setOpenEditPayment(true);
    prepareEditField(paymentId);
  };

  const handleAdd = () => {
    setOpenAddPayment(true);
    clearFields();
    clearErrorInfo();
  };

  const refreshPage = () => {
    AdminServices.getPaymentsAdmin()
      .then((res) => {
        let result = res?.data || [];
        let rows = result.map((payment) => convertPaymentToRow(payment));
        setPaymentRows(rows);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    refreshPage();
  }, [AdminServices, pageSize, currentPage]);

  const clearFields = () => {
    setEditedId("");
    setPaymentName("");
    setImage(null);
    setEditImage(null);
    setPaymentActive(false);
  };

  const clearErrorInfo = () => {
    setFieldErrors({
      Name: [],
      Image: [],
    });
    setNonFieldErrors(false);
  };

  const handleAddPayment = () => {
    const payload = {
      name: paymentName,
      image,
    };
    AdminServices.addPayment(payload)
      .then((res) => {
        setOpenAddPayment(false);
        refreshPage();
      })
      .catch((err) => {
        console.log(err);
        const status = err.response?.status;
        if (status === 400) {
          const errors = err.response?.data?.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  const handleEditPayment = () => {
    const payload = {
      name: paymentName,
      image: editImage,
      inactive: !paymentActive,
    };
    AdminServices.editPayment(editedId, payload)
      .then((res) => {
        setOpenEditPayment(false);
        refreshPage();
        clearFields();
      })
      .catch((err) => {
        console.log(err);
        const status = err.response?.status;
        if (status === 400) {
          const errors = err.response?.data?.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AdminUtility open={open} toggleDrawer={toggleDrawer} />
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Grid sx={{ padding: "0.75rem" }} item xs={12} md={12} lg={12}>
                <Grid
                  item
                  container
                  justifyContent={"space-between"}
                  md={12}
                  marginY={3}
                >
                  <Typography variant="h4">
                    Payment Method Management
                  </Typography>
                  <Button
                    variant="contained"
                    onClick={handleAdd}
                    sx={{
                      bgcolor: "#F2C94C",
                      "&:hover": { bgcolor: "#FFCD38" },
                    }}
                  >
                    Add Payment
                  </Button>
                </Grid>
                <AppTable columnsLabel={columnTable} rows={paymentRows} />
              </Grid>

              {/* Recent Deposits */}
              {/* Recent Orders */}
              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                  {/* <Orders /> */}
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>

      {/* START: Form Edit Payment */}
      <CommonDialog
        open={openEditPayment}
        onClose={() => setOpenEditPayment(false)}
        onSubmit={handleEditPayment}
        title={"Edit Payment "}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            padding: "0.75rem",
            gap: 5,
          }}
        >
          <TextField
            label="Name"
            variant="outlined"
            type="text"
            fullWidth
            value={paymentName}
            onChange={(e) => setPaymentName(e.target.value)}
            error={fieldErrors.Name.length !== 0}
            helperText={fieldErrors.Name[0]}
          />
          <Box
            component={"img"}
            src={
              typeof editImage == "string" || editImage == null
                ? editImage
                : URL.createObjectURL(editImage)
            }
            sx={{
              height: "5rem",
              width: "5rem",
              minHeight: "4rem",
              border: "1px solid gray",
              borderRadius: "0.5rem",
            }}
          />
          <TextField
            variant="outlined"
            type="file"
            fullWidth
            onChange={(e) => setEditImage(e.currentTarget.files[0])}
            error={fieldErrors.Image.length !== 0}
            helperText={fieldErrors.Image[0]}
          />
          <Box marginY={2}>
            <Switch
              checked={paymentActive}
              onClick={() => setPaymentActive(!paymentActive)}
              color="primary"
            />
            {paymentActive ? "Active" : "Inactive"}
          </Box>
        </Box>
      </CommonDialog>
      {/* END: Handle edit payment */}

      {/* START: Handle add payment */}
      <CommonDialog
        open={openAddPayment}
        onClose={() => setOpenAddPayment(false)}
        onSubmit={handleAddPayment}
        title={"Tambah payment method"}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            padding: "0.75rem",
            gap: 5,
          }}
        >
          <TextField
            label="Name"
            variant="outlined"
            type="text"
            fullWidth
            value={paymentName}
            onChange={(e) => setPaymentName(e.target.value)}
            error={fieldErrors.Name.length !== 0}
            helperText={fieldErrors.Name[0]}
          />
          <Box
            component={"img"}
            src={image && URL.createObjectURL(image)}
            sx={{
              height: "5rem",
              width: "5rem",
              minHeight: "4rem",
              border: "1px solid gray",
              borderRadius: "0.5rem",
            }}
          />
          <TextField
            variant="outlined"
            type="file"
            fullWidth
            onChange={(e) => setImage(e.currentTarget.files[0])}
            error={fieldErrors.Image.length !== 0}
            helperText={fieldErrors.Image[0]}
          />
        </Box>
      </CommonDialog>
      {/* START: Handle add payment */}
    </>
  );
}
