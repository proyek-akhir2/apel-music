import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Button } from "@mui/material";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { useEffect, useState } from "react";
import AppTable from "../../assets/components/AppTable";
import AdminUtility from "../../assets/components/admin/AdminUtility";
import Copyright from "../../assets/components/admin/Copyright";
import { useApiContext } from "../../context/ApiProvider";
import { formatDate } from "../../utility/dateFormat";
import { rupiah } from "../../utility/formatIDR";

export default function AdminInvoice() {
  const [open, setOpen] = React.useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const [add, setAdd] = React.useState(false);

  const columnsLabel = [
    "No",
    "No. Invoice",
    "Tanggal Beli",
    "Jumlah Kursus",
    "Total Harga",
    "Action",
  ];

  const [invoices, setInvoices] = useState([
    {
      id: 0,
      invoiceNumber: "",
      userId: "",
      purchaseDate: "",
      quantity: 0,
      totalPrice: 0,
      paymentId: "",
      payment: {
        id: "",
        image: null,
        name: "",
        inactive: "",
      },
    },
  ]);

  const [invoiceRows, setInvoiceRows] = useState([
    {
      No: 0,
      NoInvoice: "",
      TglBeli: "",
      JmlKursus: 0,
      TotalHarga: rupiah(0),
      Btn: (
        <>
          <Button
            variant="contained"
            color="primary"
            sx={{
              width: "180px",
              borderRadius: "0.5rem",
              fontSize: "1rem",
              fontWeight: 500,
            }}
            href="/invoice/rincian-invoice/"
          >
            Rincian
          </Button>
        </>
      ),
    },
  ]);

  const { AdminServices, URLs } = useApiContext();

  const [pageSize, setPageSize] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const convertInvoiceToRows = (invoice) => {
    return {
      No: invoice?.id,
      NoInvoice: invoice?.invoiceNumber,
      TglBeli: formatDate(invoice?.purchaseDate),
      JmlKursus: invoice?.quantity,
      TotalHarga: rupiah(invoice?.totalPrice),
      Btn: (
        <>
          <Button
            variant="contained"
            color="primary"
            sx={{
              width: "180px",
              borderRadius: "0.5rem",
              fontSize: "1rem",
              fontWeight: 500,
            }}
            href={"/invoice/rincian-invoice/" + invoice?.id}
          >
            Rincian
          </Button>
        </>
      ),
    };
  };

  useEffect(() => {
    setInvoiceRows([]);
    setInvoices([]);
    let params = {
      PageSize: pageSize,
      CurrentPage: currentPage,
    };
    AdminServices.getAdminInvoices(params)
      .then((res) => {
        let result = res.data.items;
        if (result) {
          let rows = result.map((inv) => convertInvoiceToRows(inv));
          setInvoiceRows(rows);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, [AdminServices, pageSize, currentPage]);

  const goLeft = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const goRight = () => {
    if (invoiceRows.length === pageSize) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AdminUtility open={open} toggleDrawer={toggleDrawer} />
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12} lg={12}>
                <Grid
                  item
                  container
                  justifyContent={"space-between"}
                  md={12}
                  marginY={3}
                >
                  <Typography variant="h4">Invoices</Typography>
                </Grid>

                {/* START: Main Table */}
                <AppTable rows={invoiceRows} columnsLabel={columnsLabel} />
                {/* END: Main Table */}

                {/* START: PAGINATION */}
                <Box
                  sx={{
                    width: "200px",
                    display: "flex",
                    justifyContent: "space-evenly",
                    height: "50px",
                    margin: "auto",
                    marginTop: "0.75rem",
                  }}
                >
                  <Button variant="outlined" onClick={goLeft}>
                    <ChevronLeftIcon />
                  </Button>
                  <Button variant="outlined" onClick={goRight}>
                    <ChevronRightIcon />
                  </Button>
                </Box>
                {/* END: PAGINATION */}
              </Grid>

              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                  {/* <Orders /> */}
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </>
  );
}
