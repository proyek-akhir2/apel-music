import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Button, Switch, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { useState } from "react";
import AppTable from "../../assets/components/AppTable";
import AdminUtility from "../../assets/components/admin/AdminUtility";
import Copyright from "../../assets/components/admin/Copyright";
import CommonDialog from "../../assets/components/dialogs/CommonDialog";
import { useApiContext } from "../../context/ApiProvider";

export default function AdminUser() {
  const [openDrawer, setOpenDrawer] = React.useState(false);

  const toggleDrawer = () => {
    setOpenDrawer(!openDrawer);
  };

  const { AdminServices, URLs } = useApiContext();

  const [users, setUsers] = useState([
    {
      id: "",
      fullName: "",
      email: "",
      inactive: "",
      createdAt: "",
      verifiedAt: null,
    },
  ]);

  const [pageSize, setPageSize] = useState(6);
  const [currentPage, setCurrentPage] = useState(1);

  const [userRows, setUserRows] = useState([]);

  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [editedId, setEditedId] = useState("");
  const [eFullName, setEFullname] = useState("");
  const [eEmail, setEEmail] = useState("");
  const [ePassword, setEPassword] = useState("");
  const [eConfirmPassword, setEConfirmPassword] = useState("");
  const [eActive, setEActive] = useState(false);

  const [openAddForm, setOpenAddForm] = useState(false);
  const [openEditForm, setOpenEditForm] = useState(false);
  const [openChagePassword, setOpenChangePassword] = useState(false);

  const handleAddUsers = () => {
    setOpenAddForm(true);
  };

  const [fieldErrors, setFieldErrors] = useState({
    FullName: [],
    Email: [],
    Password: [],
    ConfirmPassword: [],
  });

  const [nonFieldErrors, setNonFieldErrors] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const convertUserToRow = (user) => {
    return {
      Name: user?.fullName,
      Email: user?.email,
      Status: (
        <Button
          variant="contained"
          color={user?.inactive ? "error" : "success"}
          sx={{ width: "5rem" }}
        >
          {user?.inactive ? "Inactive" : "Active"}
        </Button>
      ),
      Action: (
        <>
          <Button
            variant="contained"
            sx={{
              minWidth: "7rem",
            }}
            onClick={() => handleEdit(user?.id)}
          >
            Edit
          </Button>
          <Button
            variant="outlined"
            sx={{
              minWidth: "4rem",
              marginLeft: "0.5rem",
            }}
            onClick={() => handleChangePassword(user?.id)}
          >
            New Pass
          </Button>
        </>
      ),
    };
  };

  const refreshPage = () => {
    let params = {
      PageSize: pageSize,
      CurrentPage: currentPage,
    };

    AdminServices.getUsers(params)
      .then((res) => {
        let result = res.data.items;
        let rows = [];
        if (result) {
          rows = result?.map((val) => convertUserToRow(val));
        }
        // TODO: lanjut bikin course add
        setUserRows(rows);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  React.useEffect(() => {
    let params = {
      PageSize: pageSize,
      CurrentPage: currentPage,
    };

    AdminServices.getUsers(params)
      .then((res) => {
        let result = res.data.items;
        let rows = [];
        if (result) {
          rows = result?.map((val) => convertUserToRow(val));
        }
        setUserRows(rows);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [AdminServices, pageSize, currentPage]);

  const handleEditUser = () => {
    clearErrorInfo();
    setIsLoading(false);

    const params = {
      fullName: eFullName,
      inactive: !eActive,
    };

    AdminServices.editUser(editedId, params)
      .then((res) => {
        let result = res?.data;
        refreshPage();
        setOpenEditForm(false);
      })
      .catch((err) => {
        console.log(err.response);
      });
  };

  // untuk set setiap field pada edit form dengan property dari user yang akan diedit
  const prepareEditField = (id) => {
    AdminServices.getUserDetail(id)
      .then((res) => {
        let result = res?.data;
        setEFullname(result?.fullName);
        setEEmail(result?.email);
        setEActive(result?.inactive == null);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleEdit = (id) => {
    clearEditFormFields();
    clearErrorInfo();
    setEditedId(id);
    setOpenEditForm(true);

    prepareEditField(id);
  };

  const handleChangePassword = (id) => {
    setEditedId(id);

    clearEditFormFields();
    clearErrorInfo();
    setOpenChangePassword(true);
    prepareEditField(id);
  };

  const handleChangePasswordUser = (id) => {
    clearErrorInfo();
    setIsLoading(false);

    const params = {
      password: ePassword,
      confirmPassword: eConfirmPassword,
    };

    AdminServices.editUserPassword(editedId, params)
      .then((res) => {
        let result = res?.data;
        setOpenChangePassword(false);
        refreshPage();
      })
      .catch((err) => {
        const status = err?.response?.status;
        if (status === 400) {
          const errors = err?.response?.data?.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  const handleAddUser = () => {
    clearErrorInfo();
    setIsLoading(false);

    AdminServices.addUser(fullName, email, password, confirmPassword)
      .then((res) => {
        setOpenAddForm(false);
        clearAddFormFields();
        setTimeout(() => {
          refreshPage();
        }, 2000);
      })
      .catch((err) => {
        const status = err.response.status;
        if (status === 400) {
          const errors = err.response.data.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  const clearAddFormFields = () => {
    setFullName("");
    setEmail("");
    setPassword("");
    setConfirmPassword("");
  };

  const clearEditFormFields = () => {
    setEFullname("");
    setEEmail("");
    setEPassword("");
    setEConfirmPassword("");
  };

  const clearErrorInfo = () => {
    setFieldErrors({
      FullName: [],
      Email: [],
      Password: [],
      ConfirmPassword: [],
    });
    setNonFieldErrors(false);
  };

  const columnTable = ["Name", "Email", "Status", "Action"];

  const goLeft = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const goRight = () => {
    if (userRows.length === pageSize) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        {/* START: Admin Navbar and Drawer */}
        <AdminUtility open={openDrawer} toggleDrawer={toggleDrawer} />
        {/* END: Admin Navbar and Drawer */}
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12} lg={12}>
                <Grid
                  item
                  container
                  justifyContent={"space-between"}
                  md={12}
                  marginY={3}
                >
                  <Typography variant="h4">Users Management</Typography>
                  <Button
                    variant="contained"
                    onClick={handleAddUsers}
                    sx={{
                      bgcolor: "#F2C94C",
                      "&:hover": { bgcolor: "#FFCD38" },
                    }}
                  >
                    Add User
                  </Button>
                </Grid>

                {/* START: MAIN TABLE */}
                <AppTable rows={userRows} columnsLabel={columnTable} />
                {/* END: MAIN TABLE */}

                {/* START: PAGINATION */}
                <Box
                  sx={{
                    width: "200px",
                    display: "flex",
                    justifyContent: "space-evenly",
                    height: "50px",
                    margin: "auto",
                    marginTop: "0.75rem",
                  }}
                >
                  <Button variant="outlined" onClick={goLeft}>
                    <ChevronLeftIcon />
                  </Button>
                  <Button variant="outlined" onClick={goRight}>
                    <ChevronRightIcon />
                  </Button>
                </Box>
                {/* END: PAGINATION */}
              </Grid>

              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                  {/* <Orders /> */}
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>

      {/* START: Add Form Dialog */}
      <CommonDialog
        open={openAddForm}
        onClose={() => setOpenAddForm(false)}
        onSubmit={handleAddUser}
        title={"Add User"}
        fullScreen={true}
      >
        <Box sx={{ minWidth: "200px" }}>
          <Grid item marginY={2}>
            <TextField
              id="Username"
              label="User Name"
              variant="outlined"
              type="text"
              fullWidth
              value={fullName}
              onChange={(e) => setFullName(e.target.value)}
              error={fieldErrors.FullName.length !== 0}
              helperText={fieldErrors.FullName[0]}
            />
          </Grid>
          <Grid item marginY={2}>
            <TextField
              id="Email"
              label="Email"
              variant="outlined"
              type="text"
              fullWidth
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              error={fieldErrors.Email.length !== 0}
              helperText={fieldErrors.Email[0]}
            />
          </Grid>
          <Grid item marginY={2}>
            <TextField
              id="Password"
              label="Password"
              variant="outlined"
              type="password"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              error={fieldErrors.Password.length !== 0}
              helperText={fieldErrors.Password[0]}
            />
          </Grid>
          <Grid item marginY={2}>
            <TextField
              id="ConfirmPassword"
              label="Confirm Password"
              variant="outlined"
              type="password"
              fullWidth
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              error={fieldErrors.ConfirmPassword.length !== 0}
              helperText={fieldErrors.ConfirmPassword[0]}
            />
          </Grid>
        </Box>
      </CommonDialog>
      {/* END: Add Form Dialog */}

      {/* START: Edit Form Dialog */}
      <CommonDialog
        open={openEditForm}
        onClose={() => setOpenEditForm(false)}
        onSubmit={handleEditUser}
        title={"Edit User : " + eEmail}
      >
        <>
          <Grid item marginY={2}>
            <TextField
              id="Username"
              label="User Name"
              variant="outlined"
              type="text"
              fullWidth
              value={eFullName}
              onChange={(e) => setEFullname(e.target.value)}
              error={fieldErrors.FullName.length !== 0}
              helperText={fieldErrors.FullName[0]}
            />
          </Grid>
          <Grid item marginY={2}>
            <Switch
              checked={eActive}
              onClick={() => setEActive(!eActive)}
              color="primary"
            />
            {eActive ? "Active" : "Inactive"}
          </Grid>
        </>
      </CommonDialog>
      {/* END: Edit Form Dialog */}

      {/* START: Change Password Form */}
      <CommonDialog
        open={openChagePassword}
        onClose={() => setOpenChangePassword(false)}
        onSubmit={handleChangePasswordUser}
        title={"Change Password user : " + eEmail}
      >
        <>
          <Grid item marginY={2}>
            <TextField
              id="Password"
              label="Password"
              variant="outlined"
              type="password"
              fullWidth
              value={ePassword}
              onChange={(e) => setEPassword(e.target.value)}
              error={fieldErrors.Password.length !== 0}
              helperText={fieldErrors.Password[0]}
            />
          </Grid>
          <Grid item marginY={2}>
            <TextField
              id="ConfirmPassword"
              label="Confirm Password"
              variant="outlined"
              type="password"
              fullWidth
              value={eConfirmPassword}
              onChange={(e) => setEConfirmPassword(e.target.value)}
              error={fieldErrors.ConfirmPassword.length !== 0}
              helperText={fieldErrors.ConfirmPassword[0]}
            />
          </Grid>
        </>
      </CommonDialog>
      {/* END: Change Password Form */}
    </>
  );
}
