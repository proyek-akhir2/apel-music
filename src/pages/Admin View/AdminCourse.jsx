import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import {
  Autocomplete,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Radio,
  TextField,
} from "@mui/material";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import * as React from "react";
import { useEffect, useState } from "react";
import AdminUtility from "../../assets/components/admin/AdminUtility";
import Copyright from "../../assets/components/admin/Copyright";
import { useApiContext } from "../../context/ApiProvider";
import { rupiah } from "../../utility/formatIDR";
import CommonDialog from "../../assets/components/dialogs/CommonDialog";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import CloseIcon from "@mui/icons-material/Close";
import noImageJpg from "../../assets/img/no-image.jpg";
import { formatDate } from "../../utility/dateFormat";

export const AdminCourseCard = ({ course, onEdit }) => {
  const { URLs } = useApiContext();

  const CardField = ({ field, value }) => {
    return (
      <>
        <Box
          sx={{
            marginBottom: "0.25rem",
          }}
        >
          <Typography color={"#969696"} fontSize={"0.75rem"} fontWeight={500}>
            {field}
          </Typography>
          <Typography color={"black"} fontSize={"1rem"} fontWeight={600}>
            {value}
          </Typography>
        </Box>
      </>
    );
  };

  return (
    <>
      <Box
        sx={{
          backgroundColor: "white",
          boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
          width: "100%",
          maxWidth: "1980px",
          display: "grid",
          gridTemplateColumns: {
            md: "repeat(1, minmax(0, 1fr))",
            lg: "repeat(3, minmax(0, 1fr))",
          },
          placeItems: "center",
          gap: 0,
          minHeight: "150px",
          paddingX: "0.5rem",
          paddingY: "1rem",
          borderLeft: "10px solid #F2C94C",
          borderRadius: "0.5rem",
        }}
      >
        <Box
          component={"img"}
          sx={{
            height: { xs: "180px", sm: "180px", md: "200px" },
          }}
          src={course?.imageName ? URLs.IMG_URL + course?.imageName : ""}
        />
        <Box
          sx={{
            marginTop: "0.5rem",
          }}
        >
          <CardField field={"ID"} value={course?.id} />
          <CardField field={"Category"} value={course?.category?.tagName} />
          <CardField field={"Name"} value={course?.name} />
          <CardField field={"Price"} value={rupiah(course?.price)} />
        </Box>
        <Box
          sx={{
            marginTop: "0.5rem",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Button
            onClick={() => onEdit(course?.id)}
            sx={{ width: "240px" }}
            variant="outlined"
          >
            Edit
          </Button>
        </Box>
      </Box>
    </>
  );
};

const CategoryList = ({ id, name, choosen, setChoosen }) => {
  const [isChoosen, setIsChoosen] = useState(false);

  useEffect(() => {
    if (choosen?.id) {
      setIsChoosen(choosen?.id === id);
    }
  }, [choosen, id]);

  const handleChoosen = () => {
    setChoosen({
      id,
      name,
    });
  };

  return (
    <>
      <ListItem
        sx={{
          padding: "0.5rem",
          cursor: "pointer",
        }}
        style={{
          backgroundColor: `${isChoosen ? "#BDBDBD" : "#FFFFFF"}`,
          borderRadius: "1rem",
        }}
        disableGutters
        onClick={handleChoosen}
      >
        <ListItemText primary={name} />
      </ListItem>
    </>
  );
};

const ScheduleList = ({ schedule, onDelete }) => {
  return (
    <>
      <ListItem
        sx={{
          paddingY: "0.5rem",
          paddingX: "1rem",
          cursor: "pointer",
          backgroundColor: "#E2E2E2",
          borderRadius: "0.25rem",
        }}
        disableGutters
        secondaryAction={
          <IconButton onClick={() => onDelete(schedule)}>
            <CloseIcon />
          </IconButton>
        }
      >
        <ListItemText primary={formatDate(schedule)} />
      </ListItem>
    </>
  );
};

// ====================== MAIN COMPONENT ===============================

export default function AdminCourse() {
  const [open, setOpen] = React.useState(false);
  const [openAddCourse, setOpenAddCourse] = useState(false);
  const [openEditCourse, setOpenEditCourse] = useState(false);
  const [openCategoryOption, setOpenCategoryOption] = useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const [courses, setCourses] = useState([
    {
      id: "",
      name: "",
      price: "",
      imageName: "",
      category: {
        id: "",
        tagName: "",
      },
    },
  ]);

  const { AdminServices, URLs } = useApiContext();

  const [pageSize, setPageSize] = useState(3);
  const [currentPage, setCurrentPage] = useState(1);

  const [fieldErrors, setFieldErrors] = useState({
    Name: [],
    CategoryId: [],
    Image: [],
    Description: [],
    Price: [],
    Schedules: [],
  });

  const [nonFieldErrors, setNonFieldErrors] = useState(false);

  const [courseId, setCourseId] = useState("");
  const [courseName, setCourseName] = useState("");
  const [courseCategory, setCourseCategory] = useState({
    id: "",
    name: "",
  });
  const [courseImage, setCourseImage] = useState("");
  const [courseDescription, setCourseDescription] = useState("");
  const [coursePrice, setCoursePrice] = useState(0);
  const [courseSchedules, setCourseSchedules] = useState([]);

  const [categoriesOption, setCategoriesOption] = useState([
    {
      id: "",
      tagName: "",
      image: "",
    },
  ]);

  const handleOpenAddForm = () => {
    setOpenAddCourse(true);
  };

  const clearFormFields = () => {
    setCourseId("");
    setCourseName("");
    setCourseCategory({ id: "", name: "" });
    setCourseImage(null);
    setCourseDescription("");
    setCourseSchedules([]);
    setCoursePrice(0);
  };

  const clearErrorFields = () => {
    setFieldErrors({
      Name: [],
      CategoryId: [],
      Image: [],
      Description: [],
      Price: [],
      Schedules: [],
    });
  };

  const refreshPage = () => {
    let params = {
      PageSize: pageSize,
      CurrentPage: currentPage,
    };

    AdminServices.getAllCourses(params)
      .then((res) => {
        let result = res.data.items;
        if (result) {
          setCourses(result);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    setCourses([]);
    refreshPage();
  }, [AdminServices, pageSize, currentPage]);

  // START: Handle Update Course

  const prepareEditfFields = (id) => {
    setCourseId(id);
    AdminServices.getCourseDetail(id)
      .then((res) => {
        // console.log(res);
        let result = res?.data;
        setCourseName(result?.name);
        setCourseCategory({
          id: result?.category?.id,
          name: result?.category?.tagName,
        });
        setCourseImage(result?.image);
        setCoursePrice(result?.price);
        setCourseDescription(result?.description);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleOpenEditCourse = (id) => {
    clearFormFields();
    clearErrorFields();
    setOpenEditCourse(true);

    prepareEditfFields(id);
  };

  const handleUpdateCourse = () => {
    const payloads = {
      name: courseName,
      categoryId: courseCategory?.id,
      image: courseImage,
      price: coursePrice,
      description: courseDescription,
    };

    AdminServices.updateCourse(courseId, payloads)
      .then((res) => {
        refreshPage();
        setOpenEditCourse(false);
        clearFormFields();
        clearErrorFields();
      })
      .catch((err) => {
        const status = err.response.status;
        if (status === 400) {
          const errors = err.response.data.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  const hanldeCloseEditCourse = () => {
    setOpenEditCourse(false);
    clearFormFields();
    clearErrorFields();
  };

  // END: Handle Update Course

  // START: Handle Add New Course
  const handleAddCourse = () => {
    const payloads = {
      name: courseName,
      categoryId: courseCategory?.id,
      image: courseImage,
      price: coursePrice,
      schedules: courseSchedules,
      description: courseDescription,
    };

    AdminServices.addCourse(payloads)
      .then((res) => {
        refreshPage();
        setOpenAddCourse(false);
        clearFormFields();
        clearErrorFields();
      })
      .catch((err) => {
        const status = err.response.status;
        if (status === 400) {
          const errors = err.response.data.errors;
          setFieldErrors({ ...fieldErrors, ...errors });
        }

        if (status > 400 && status < 500) {
          const errors = err?.response?.data;
          setNonFieldErrors(errors);
        }
      });
  };

  const hanldeCloseAddCourse = () => {
    setOpenAddCourse(false);
    clearFormFields();
    clearErrorFields();
  };
  // END: Handle Add New Course

  const handleOpenCategoryOption = () => {
    AdminServices.getAllCategories()
      .then((res) => {
        let categories = res.data;
        setOpenCategoryOption(true);
        setCategoriesOption(categories);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  // START: Handle Schedule
  const scheduleRef = React.useRef();

  const scheduleAlreadyIn = (schedule) => {
    let result = courseSchedules.findIndex((val) => val.match(schedule));
    return result > -1;
  };

  const handleAddSchedule = () => {
    let schedule = scheduleRef?.current?.value;
    let alreadyIn = scheduleAlreadyIn(schedule);
    if (!alreadyIn && schedule) {
      schedule = new Date(schedule).toISOString();
      let temp = [...courseSchedules, schedule];
      temp.sort();
      setCourseSchedules(temp);
    }
  };

  const handleDeleteSchedule = (schedule) => {
    let result = courseSchedules.filter((val) => val !== schedule);
    setCourseSchedules(result);
  };

  // END: Handle Schedule

  const goLeft = () => {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const goRight = () => {
    if (courses.length === pageSize) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AdminUtility open={open} toggleDrawer={toggleDrawer} />
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12} lg={12}>
                <Grid
                  item
                  container
                  justifyContent={"space-between"}
                  md={12}
                  marginY={3}
                >
                  <Typography variant="h4">Courses</Typography>
                  <Button variant="contained" onClick={handleOpenAddForm}>
                    Add Course
                  </Button>
                </Grid>

                {/* START: Main Content */}
                <Box
                  sx={{
                    width: "100%",
                    minHeight: "200px",
                    display: "flex",
                    flexDirection: "column",
                    gap: 2,
                  }}
                >
                  {courses.map((c, i) => (
                    <AdminCourseCard
                      key={i}
                      course={c}
                      onEdit={handleOpenEditCourse}
                    />
                  ))}
                </Box>
                {/* END: Main Content */}

                {/* START: PAGINATION */}
                <Box
                  sx={{
                    width: "200px",
                    display: "flex",
                    justifyContent: "space-evenly",
                    height: "50px",
                    margin: "auto",
                    marginTop: "0.75rem",
                  }}
                >
                  <Button variant="outlined" onClick={goLeft}>
                    <ChevronLeftIcon />
                  </Button>
                  <Button variant="outlined" onClick={goRight}>
                    <ChevronRightIcon />
                  </Button>
                </Box>
                {/* END: PAGINATION */}
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>

      {/* START: Add New Course Dialog */}
      <CommonDialog
        open={openAddCourse}
        onSubmit={handleAddCourse}
        onClose={hanldeCloseAddCourse}
        fullScreen={true}
        title={"Tambah Course Baru"}
      >
        <TextField
          fullWidth
          variant="outlined"
          label="Name"
          value={courseName}
          onChange={(e) => setCourseName(e.target.value)}
          sx={{ marginBottom: "1rem" }}
          error={fieldErrors.Name.length !== 0}
          helperText={fieldErrors.Name[0]}
        />
        <TextField
          fullWidth
          variant="outlined"
          label="Category"
          value={courseCategory?.name}
          sx={{
            cursor: "pointer",
            marginBottom: "1rem",
          }}
          onClick={handleOpenCategoryOption}
          InputProps={{
            readOnly: true,
            endAdornment: (
              <InputAdornment position="start" sx={{ cursor: "pointer" }}>
                <FormatListBulletedIcon />
              </InputAdornment>
            ),
          }}
          error={fieldErrors.CategoryId.length !== 0}
          helperText={fieldErrors.CategoryId[0]}
        />
        <Typography color={"#969696"} fontSize={"1rem"} fontWeight={400}>
          Image
        </Typography>
        <Box
          component={"img"}
          src={
            courseImage && openAddCourse
              ? URL.createObjectURL(courseImage)
              : noImageJpg
          }
          sx={{
            width: "20rem",
            height: "13.5rem",
            border: "1px solid gray",
            borderRadius: "1rem",
            marginBottom: "1rem",
            objectFit: "contain",
            backgroundColor: `${courseImage ? "#000000" : "#EEEEEE"}`,
          }}
        />
        <TextField
          variant="outlined"
          type="file"
          fullWidth
          sx={{
            marginBottom: "1rem",
          }}
          onChange={(e) => setCourseImage(e.currentTarget.files[0])}
          error={fieldErrors.Image.length !== 0}
          helperText={fieldErrors.Image[0]}
        />
        <TextField
          fullWidth
          label="Price"
          value={coursePrice}
          onChange={(e) => {
            setCoursePrice((v) => (e.target.validity ? e.target.value : v));
          }}
          inputProps={{
            inputMode: "numeric",
            pattern: /[0-9]*/g,
          }}
          sx={{
            marginBottom: "1rem",
          }}
          error={fieldErrors.Price.length !== 0}
          helperText={fieldErrors.Price[0]}
        />

        <Typography color={"#969696"} fontSize={"1rem"} fontWeight={400}>
          Schedule
        </Typography>
        <Box
          sx={{
            maxWidth: "30rem",
            border: "1px solid gray",
            marginBottom: "1rem",
            minHeight: "10rem",
            borderRadius: "0.25rem",
            padding: "0.5rem",
            display: "flex",
            flexDirection: "column",
            gap: 1,
          }}
        >
          {courseSchedules.map((schedule, i) => (
            <ScheduleList
              key={i}
              schedule={schedule}
              onDelete={handleDeleteSchedule}
            />
          ))}
        </Box>
        <Box
          sx={{
            width: "100%",
            display: "flex",
            gap: 2,
            alignItems: "center",
            marginBottom: "1rem",
          }}
        >
          <TextField
            fullWidth
            variant="outlined"
            type="datetime-local"
            sx={{ marginBottom: "1rem" }}
            error={fieldErrors.Schedules.length !== 0}
            helperText={fieldErrors.Schedules[0]}
            inputRef={scheduleRef}
          />
          <Button
            variant="contained"
            onClick={handleAddSchedule}
            sx={{ paddingY: "1rem" }}
          >
            Add
          </Button>
        </Box>
        <TextField
          fullWidth
          multiline
          rows={4}
          variant="outlined"
          label="Description"
          value={courseDescription}
          onChange={(e) => setCourseDescription(e.target.value)}
          sx={{ marginBottom: "1rem" }}
          error={fieldErrors.Description.length !== 0}
          helperText={fieldErrors.Description[0]}
        />
      </CommonDialog>
      {/* END: Add New Course Dialog */}

      {/* START: Edit Course Dialog */}
      <CommonDialog
        open={openEditCourse}
        onSubmit={handleUpdateCourse}
        onClose={hanldeCloseEditCourse}
        fullScreen={true}
        title={"Edit Course"}
      >
        <TextField
          fullWidth
          variant="outlined"
          label="Name"
          value={courseName}
          onChange={(e) => setCourseName(e.target.value)}
          sx={{ marginBottom: "1rem" }}
          error={fieldErrors.Name.length !== 0}
          helperText={fieldErrors.Name[0]}
        />
        <TextField
          fullWidth
          variant="outlined"
          label="Category"
          value={courseCategory?.name}
          sx={{
            cursor: "pointer",
            marginBottom: "1rem",
          }}
          onClick={handleOpenCategoryOption}
          InputProps={{
            readOnly: true,
            endAdornment: (
              <InputAdornment position="start" sx={{ cursor: "pointer" }}>
                <FormatListBulletedIcon />
              </InputAdornment>
            ),
          }}
          error={fieldErrors.CategoryId.length !== 0}
          helperText={fieldErrors.CategoryId[0]}
        />
        <Typography color={"#969696"} fontSize={"1rem"} fontWeight={400}>
          Image
        </Typography>
        <Box
          component={"img"}
          src={
            courseImage && openEditCourse // apkah courseImage ada dan sedang Open Edit ?
              ? typeof courseImage == "object" // apakah courseImage berupa object dari file input atau string?
                ? URL.createObjectURL(courseImage)
                : URLs.IMG_URL + courseImage
              : noImageJpg
          }
          sx={{
            width: "20rem",
            height: "13.5rem",
            border: "1px solid gray",
            borderRadius: "1rem",
            marginBottom: "1rem",
            objectFit: "contain",
            backgroundColor: `${courseImage ? "#000000" : "#EEEEEE"}`,
          }}
        />
        <TextField
          variant="outlined"
          type="file"
          fullWidth
          sx={{
            marginBottom: "1rem",
          }}
          onChange={(e) => setCourseImage(e.currentTarget.files[0])}
          error={fieldErrors.Image.length !== 0}
          helperText={fieldErrors.Image[0]}
        />
        <TextField
          fullWidth
          label="Price"
          value={coursePrice}
          onChange={(e) => {
            setCoursePrice((v) => (e.target.validity ? e.target.value : v));
          }}
          inputProps={{
            inputMode: "numeric",
            pattern: /[0-9]*/g,
          }}
          sx={{
            marginBottom: "1rem",
          }}
          error={fieldErrors.Price.length !== 0}
          helperText={fieldErrors.Price[0]}
        />
        <TextField
          fullWidth
          multiline
          rows={4}
          variant="outlined"
          label="Description"
          value={courseDescription}
          onChange={(e) => setCourseDescription(e.target.value)}
          sx={{ marginBottom: "1rem" }}
          error={fieldErrors.Description.length !== 0}
          helperText={fieldErrors.Description[0]}
        />
      </CommonDialog>
      {/* END: Edit Course Dialog */}

      {/* START: Dialog untuk pilih category */}
      <CommonDialog
        open={openCategoryOption}
        onClose={() => setOpenCategoryOption(false)}
        onSubmit={() => setOpenCategoryOption(false)}
        fullScreen={false}
        title={"Pilih Category"}
      >
        <List sx={{ pt: 0 }}>
          {categoriesOption.map((item, i) => (
            <CategoryList
              key={i}
              id={item?.id}
              name={item?.tagName}
              choosen={courseCategory}
              setChoosen={setCourseCategory}
            />
          ))}
        </List>
      </CommonDialog>
      {/* END: Dialog untuk pilih category */}
    </>
  );
}
