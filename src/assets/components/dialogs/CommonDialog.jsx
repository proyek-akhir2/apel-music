import {
  AppBar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Slide,
  Toolbar,
  Typography,
} from "@mui/material";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";

const DialogTransition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DialogBar = ({ title, onClose, onSubmit }) => {
  return (
    <>
      <AppBar sx={{ position: "relative", backgroundColor: "#F2C94C" }}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={onClose}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
            {title}
          </Typography>
          <Button
            variant="contained"
            autoFocus
            color="primary"
            onClick={onSubmit}
          >
            save
          </Button>
        </Toolbar>
      </AppBar>
    </>
  );
};

const CommonDialog = ({
  title,
  open,
  onClose,
  onSubmit,
  fullScreen,
  children,
}) => {
  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={onClose}
      TransitionComponent={DialogTransition}
    >
      {fullScreen ? (
        <>
          <DialogBar onClose={onClose} title={title} onSubmit={onSubmit} />
        </>
      ) : (
        <>
          <DialogTitle>{title}</DialogTitle>
        </>
      )}
      <>
        <DialogContent
          sx={{
            minWidth: "25rem",
            padding: "0.75rem",
            // Style dibawah hanya dijalankan jika dialog fullScreen
            ...(fullScreen && {
              marginTop: "2rem",
              marginX: { xs: "0.125rem", sm: "0.125rem", md: "1rem" },
            }),
          }}
        >
          {children}
        </DialogContent>

        {fullScreen || (
          <>
            <DialogActions>
              <Button onClick={onClose} sx={{ color: "black" }}>
                Cancel
              </Button>
              <Button
                onClick={onSubmit}
                variant="contained"
                sx={{
                  bgcolor: "#F2C94C",
                  "&:hover": { bgcolor: "#FFCD38" },
                }}
              >
                Save
              </Button>
            </DialogActions>
          </>
        )}
      </>
    </Dialog>
  );
};

export default CommonDialog;
