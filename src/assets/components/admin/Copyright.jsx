import { Link, Typography } from "@mui/material";
import React from "react";
import { useApiContext } from "../../../context/ApiProvider";

const Copyright = (props) => {
  const { URLs } = useApiContext();

  return (
    <>
      <Typography
        variant="body2"
        color="text.secondary"
        align="center"
        {...props}
      >
        {"Copyright © "}
        <Link color="inherit" href={URLs.BASE_URL}>
          Apel Music
        </Link>{" "}
        {new Date().getFullYear()}
        {"."}
      </Typography>
    </>
  );
};

export default Copyright;
